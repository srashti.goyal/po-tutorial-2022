Posterior overlap analysis is used for searching for strongly lensed binary merger events in the LVK. This repo contains examples for the simulated and real events analysis. The introductory slides for the method can be found [here](https://docs.google.com/presentation/d/10h4rjnX2k6S8S2xyzZ9VVY0AfAR7YcAWXFZsJqMG-Ew/edit?usp=sharing).
