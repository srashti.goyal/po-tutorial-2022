import numpy as np, sys, os
from scipy.stats import gaussian_kde
import scipy.ndimage.filters as filter
from scipy import interpolate

class bayes_factor(object):
    """
    This class takes the posterior samples for two events
    and calculate bayes factors B_lu (see arXiv:1807.07062)
    Input parameters:
    posterior_samples1: posterior samples from event 1 after reading from posterior file
    posterior_samples2: posterior samples from event 2 after reading from posterior file
    """

    def __init__(self,posterior_samples1,posterior_samples2,flatprior=True):
        self.posterior_samples1 = posterior_samples1
        self.posterior_samples2 = posterior_samples2
        self.flatprior=flatprior

    def compute_skyoverlap(self,n=500):
        ra1,sindec1 = self.posterior_samples1['ra'], np.cos(np.pi/2.-self.posterior_samples1['dec'])
        ra2,sindec2 = self.posterior_samples2['ra'], np.cos(np.pi/2.-self.posterior_samples2['dec'])
        ra_b = np.linspace(0,2*np.pi,n)
        sdec_b = np.linspace(-1,1,n)
        d_ra = ra_b[1]-ra_b[0]
        d_sdec = sdec_b[1]-sdec_b[0]
        h1, xedges, yedges = np.histogram2d(ra1, sindec1,bins=(ra_b, sdec_b),normed=True)
        h2, xedges, yedges = np.histogram2d(ra2, sindec2,bins=(ra_b, sdec_b),normed=True)
        return np.sum(np.sum(h1*h2))*d_ra*d_sdec

    def calc_blu(self,params,prange_low,prange_up):
        """
        This function calculate bayes factor B_lu (see arXiv:1807.07062)
        for lensed vs unlensed hypothesis
        Input parameters:
        params: list of parameters for which B_lu is to be calculated
        prange_low: list of lower limit for each parameters
        prange_upL list of upper limit for each parameters
        """
        if 'ra' in params and 'dec' in params:
            params_kde = [e for e in params if e not in ('ra','dec')]
            #print(params_kde)
            skyoverlap = self.compute_skyoverlap()
            #print(skyoverlap)
        else:
            params_kde = params.copy()
            skyoverlap = 1.
        data1 = np.array([self.posterior_samples1[item] for item in params_kde])
        data2 = np.array([self.posterior_samples2[item] for item in params_kde])

        if 'dec' in params_kde:
            print('Test---')
            indx = params.index('dec')
            data1[indx] = np.cos(np.pi/2.-self.posterior_samples1['dec'])
            data2[indx] = np.cos(np.pi/2.-self.posterior_samples2['dec'])
        if not params_kde:
            return skyoverlap
        KDE1 = gaussian_kde(data1)
        KDE2 = gaussian_kde(data2)
        KDE1.set_bandwidth(bw_method=KDE1.factor / 4.)
        KDE2.set_bandwidth(bw_method=KDE2.factor / 4.)
        if self.flatprior is True:
            #print(KDE1.integrate_kde(KDE2))
            prior_scaling = np.product([item1 - item2 for item1,item2 in zip(prange_up,prange_low)])
            return KDE1.integrate_kde(KDE2)*prior_scaling*skyoverlap
        else:
            return KDE1.integrate_kde(KDE2)*skyoverlap

        

        
class rlu(object):
    """
    This class takes the posterior samples for two events
    and calculate bayes factors R_lu (see arXiv:1807.07062)
    Input parameters:
    tc_posteriors_1: time posterior samples from event 1 after reading from posterior file
    tc_posteriors_2: time posterior samples from event 2 after reading from posterior file
    t1= GPS time in seconds of first event
    t2= GPS time in seconds for the second event

    """

    def __init__(self,tc_posteriors_1=[],tc_posteriors_2=[],t1=None,t2=0.):
  

        if t1!=None:
            self.t1 = t1
            self.t2 = t2
        else:
            self.argmax_t1_indx = np.argmax(np.histogram(tc_posteriors_1,bins=500)[0])
            self.argmax_t2_indx = np.argmax(np.histogram(tc_posteriors_2,bins=500)[0])
            self.t1 = np.histogram(tc_posteriors_1,bins=500)[1][self.argmax_t1_indx]
            self.t2 = np.histogram(tc_posteriors_2,bins=500)[1][self.argmax_t2_indx]



    def p_log10dt(self,dt,p_t,t):
        self.p_log10dt_interp = interpolate.interp1d(t, p_t*t/np.log10(np.e))
        self.tmax_interp = max(t)
        if dt<self.tmax_interp:
            p = self.p_log10dt_interp(dt)
        else:
            p = 0
        return p

    def ulensed_log10dt_prior_pdf(self,T):
        """
        This function calculate prior pdf for unlensed events
        input parameters:
         T: Total observation time (in seconds)
        """
        dt = np.abs(self.t1-self.t2)
        return 2*(T-dt)*dt/np.power(T,2)/np.log10(np.e)


    def calc_rlu_pt(self,t,p_t,run_time_detector):
        """
        This function calculates lensing Bayes factor from time delay: Rlu (see arXiv:1807.07062)
        for lensed vs unlensed hypothesis
        Input parameters:
        t: time delay in seconds for SIS lens
        p_t: probability for a time delay for SIS lens
        run_time_detector: Total observation time (in seconds)        
        """
        dt = np.abs(self.t1-self.t2)
        rlu_pt = self.p_log10dt(dt,t,p_t) / self.ulensed_log10dt_prior_pdf(run_time_detector)
        return rlu_pt

    def calc_rlu_dt_samples(self,dt_samples,run_time_detector):
        """
        This function calculates lensing Bayes factor from time delay: Rlu (see arXiv:1807.07062)
        for lensed vs unlensed hypothesis
        Input parameters:
        dt_samples: dt samples for SIE lens (seconds)(KDE method)

        run_time_detector: Total observation time (in seconds)  

        """

        kde_lensed_log10dt = gaussian_kde(np.log10(dt_samples),bw_method=0.1)

        dt = np.abs(self.t1-self.t2)
        rlu_dt_samples=(kde_lensed_log10dt(np.log10(dt)))/( self.ulensed_log10dt_prior_pdf(run_time_detector))
        return rlu_dt_samples

